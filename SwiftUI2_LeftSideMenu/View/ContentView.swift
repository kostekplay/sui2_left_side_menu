////  ContentView.swift
//  SwiftUI2_LeftSideMenu
//
//  Created on 16/03/2021.
//  
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var homeViewModel = HomeViewModel()
    
    var body: some View {
        
        ZStack {
            VStack(spacing: 10, content: {
                
                HStack(spacing: 20, content: {
                    Button(action: {
                        withAnimation(.easeIn){
                            homeViewModel.showMenu.toggle() // true
                        }
                    }, label: {
                        Image(systemName: "line.horizontal.3")
                            .font(.title)
                            .foregroundColor(.orange)
                    })
                    
                    Spacer()
                })
                .padding()
                .frame(maxWidth: .infinity)
                .background(Color.gray.opacity(0.4).ignoresSafeArea())
                
                Spacer()
                
            })
            .frame(maxWidth: .infinity,maxHeight: .infinity)
            .background(Color.gray.opacity(0.4).ignoresSafeArea())
            
            HStack {
                LeftSideMenuView(homeViewModel: homeViewModel)
                    .offset(x: homeViewModel.showMenu ? 0 : -UIScreen.main.bounds.width / 1.6, y: 10)
                
                Spacer()
            }
            .background(Color.black.opacity(homeViewModel.showMenu ? 0.3 : 0).ignoresSafeArea())
            .onTapGesture {
                withAnimation(.easeOut){homeViewModel.showMenu.toggle()} // false
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
