////  LeftSideMenuView.swift
//  SwiftUI2_LeftSideMenu
//
//  Created on 16/03/2021.
//  
//

import SwiftUI

struct LeftSideMenuView: View {
    
    @ObservedObject var homeViewModel: HomeViewModel
    
    var body: some View {
        
        VStack {
            HStack {
                Image(systemName: "cart")
                    .font(.title)
                    .foregroundColor(Color.orange)
                
                Spacer()
            }
            Spacer()
        }
        .padding()
        .frame(width: UIScreen.main.bounds.width / 1.6)
        .background(Color.white.ignoresSafeArea())
    }
}

struct LeftSideMenuView_Previews: PreviewProvider {
    static var previews: some View {
        let homeViewModel = HomeViewModel()
        LeftSideMenuView(homeViewModel: homeViewModel)
    }
}

