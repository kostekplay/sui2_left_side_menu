////  SwiftUI2_LeftSideMenuApp.swift
//  SwiftUI2_LeftSideMenu
//
//  Created on 16/03/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_LeftSideMenuApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
